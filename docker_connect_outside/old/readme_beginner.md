# Check IP
## linux
```
ifconfig

usb0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.42.155  netmask 255.255.255.0  broadcast 192.168.42.255
        inet6 fe80::b343:f91:5e5e:f4b9  prefixlen 64  scopeid 0x20<link>
        ether 6a:9d:9e:3a:18:97  txqueuelen 1000  (Ethernet)
        RX packets 13208  bytes 13259327 (13.2 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 10698  bytes 1871307 (1.8 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

## windows
```
ipconfig

Ethernet adapter vEthernet (External Virtual Network):

   Connection-specific DNS Suffix  . : home
   Link-local IPv6 Address . . . . . : fe80::c542:1540:8737:c9cd%11
   IPv4 Address. . . . . . . . . . . : 192.168.1.3
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . : 192.168.1.1
```

# Testing out
## Start the Alpine container and drop into a Shell prompt.
`docker container run --rm -it alpine sh`

## Install the ping utility.
`apk update && apk add iputils`

## Ping your local network IP address (replace my IP address with yours).
`ping 192.168.1.3`

## You should see this output (hit CTRL+C to stop it):
```
PING 192.168.1.3 (192.168.1.3) 56(84) bytes of data.
64 bytes from 192.168.1.3: icmp_seq=1 ttl=37 time=0.539 ms
64 bytes from 192.168.1.3: icmp_seq=2 ttl=37 time=0.417 ms
64 bytes from 192.168.1.3: icmp_seq=3 ttl=37 time=0.661 ms
```