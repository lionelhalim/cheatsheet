What if local ip address changed? we need to use custom bridge docker network

# Create a custom bridge network
```
docker network create -d bridge --subnet 192.168.0.0/24 --gateway 192.168.0.1 mynet
```
Feel free to change around the IP addresses and mynet name if you want. In the end, after running this command you’ll be able to access your Docker host by the IP address of 192.168.0.1 regardless of what your real local IP address is.

# Testing it out
```
# Start the Alpine container, but this time we'll use our custom network.
docker container run --rm -it --net=mynet alpine sh

# Install the ping utility.
apk update && apk add iputils

# Ping the custom IP address we set up.
ping 192.168.0.1

# You should see this output (hit CTRL+C to stop it):
PING 192.168.0.1 (192.168.0.1) 56(84) bytes of data.
64 bytes from 192.168.0.1: icmp_seq=1 ttl=64 time=0.053 ms
64 bytes from 192.168.0.1: icmp_seq=2 ttl=64 time=0.119 ms
64 bytes from 192.168.0.1: icmp_seq=3 ttl=64 time=0.062 ms
```

