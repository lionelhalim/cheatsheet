Example supervisor config
```
[group:rmq-consumer]
programs=rmq-consumer1,rmq-consumer2,rmq-consumer3

[program:rmq-consumer1]
process_name=%(program_name)s%(process_num)s
directory=/home/lionel-private/self_project/laravel_with_rabbitmq
command=php artisan rmq:consume queue1
numprocs=2
stopwaitsecs=60

[program:rmq-consumer2]
process_name=%(program_name)s%(process_num)s
directory=/home/lionel-private/self_project/laravel_with_rabbitmq
command=php artisan rmq:consume queue2
numprocs=2
stopwaitsecs=60

[program:rmq-consumer3]
process_name=%(program_name)s%(process_num)s
directory=/home/lionel-private/self_project/laravel_with_rabbitmq
command=php artisan rmq:consume queue3
numprocs=2
stopwaitsecs=60
```

Dengan menggunakan `stopwaitsecs` supervisor akan menunggu penyetopan program. Oleh karena itu, `stopwaitsecs` harus diisi dengan waktu terlama proses bisa selesai. Jika melebihi valuenya, program akan tetap dihentikan secara paksa.