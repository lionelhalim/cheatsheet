# Tutorial
- https://nickjanetakis.com/blog/docker-tip-35-connect-to-a-database-running-on-your-docker-host
- https://mariadb.com/kb/en/configuring-mariadb-for-remote-client-access/
- https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach

# Docs
- What is docker network bridge? https://docs.docker.com/network/bridge/#:~:text=In%20terms%20of%20Docker%2C%20a,connected%20to%20that%20bridge%20network.&text=Bridge%20networks%20apply%20to%20containers%20running%20on%20the%20same%20Docker%20daemon%20host.
- What is docker network host? https://docs.docker.com/network/host/#:~:text=The%20host%20networking%20driver%20only,the%20docker%20service%20create%20command.

# Firewall
## Linux
- https://www.cyberciti.biz/faq/how-to-block-an-ip-address-with-ufw-on-ubuntu-linux-server/
- https://serverfault.com/questions/484475/ufw-firewall-rules-ordering
## Windows
- https://superuser.com/questions/1592265/
what-is-the-equivalent-of-ufw-in-windows