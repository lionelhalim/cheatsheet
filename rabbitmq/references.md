# Learning lesson
- RabbitMQ : Message Queues for beginners: https://www.youtube.com/watch?v=hfUIWe1tK8E&t=802s

- RabbitMQ : How to setup a RabbitMQ cluster - for beginners: https://www.youtube.com/watch?v=FzqjtU2x6YA

- Using RabbitMQ with GO #GoJakartaMeetup: https://www.youtube.com/watch?v=l_SgYpDu3JQ

- How to Set Up Laravel with Docker | Laravel Microservices #1: https://www.youtube.com/watch?v=-vc9Hfeg2eQ

# Docs
- Docker swarm with secret
    - https://docs.docker.com/engine/reference/commandline/service_create/#create-a-service-with-secrets

- RabbitMQ Clustering
    - https://www.rabbitmq.com/clustering.html
    - For clustering with auto discovery
        - https://www.rabbitmq.com/configure.html

- RabbitMQ Mirroring
    - Tujuannya untuk high availability (ha)
    - https://www.rabbitmq.com/ha.html
    - Can be applied with policy: https://www.rabbitmq.com/parameters.html#policies

# Docker Images
- RabbitMQ
    - https://hub.docker.com/_/rabbitmq

# Source Code
- https://github.com/marcel-dempers/docker-development-youtube-series/tree/master/messaging/rabbitmq