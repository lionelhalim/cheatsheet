# Beginner explanation for rabbitmq with docker

## Running the docker
> It is always recommended to explicitly give hostname for easier tracking

- Parameter Explanation
    - -d is running docker in background
    - --hostname / -h is the hostname for rabbitmq
    - --name is docker container name that is running
    - rabbitmq version 
        - rabbitmq:3.8 is the image that will be used for running rabbitmq
        - rabbitmq:3.8-alpine; alpine is smaller version
```
docker run -d --hostname my-rabbit --name some-rabbit rabbitmq:3.8
docker run -d --hostname my-rabbit --name some-rabbit rabbitmq:3.8-alpine
```

## Defining Port
- add flag -p to bind the pc port to container port
    - port 15672 is default port for rabbitmq management

```
docker run -d --hostname my-rabbit --name some-rabbit -p 8080:15672 rabbitmq:3-management
```

### Showing logs
```
sudo docker logs container-name
```