Creating function to receive `pcntl_signal_dispatch()`
```
$signalHandler = function ($signo) use (&$running) {
    $running = false;
    Log::info("Received SIGTERM signal: $signo");
};
```

The first pcntl_signal is assigning callback with our custom callback for the respective signal

The second pcntl_signal is reverting callback to system when our program exited.
```
# async caller here

pcntl_signal(SIGTERM, $signalHandler);
pcntl_signal(SIGHUP, $signalHandler);
pcntl_signal(SIGUSR1, $signalHandler);

# infinite looper here

pcntl_signal(SIGTERM, SIG_DFL);
pcntl_signal(SIGHUP, SIG_DFL);
pcntl_signal(SIGUSR1, SIG_DFL);
```

SIG_BLOCK will blocking signal yang di list
SIG_UNBLOCK akan unblock signal yang di list
```
# asynchronous programs

pcntl_sigprocmask(SIG_BLOCK, [SIGTERM, SIGHUP, SIGUSR1, SIGKILL]);

// critical logic here

pcntl_sigprocmask(SIG_UNBLOCK, [SIGTERM, SIGHUP, SIGUSR1, SIGKILL]);
pcntl_signal_dispatch();
```