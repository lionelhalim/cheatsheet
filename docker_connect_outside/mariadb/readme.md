# Option 1: with docker bridge network
https://docs.docker.com/network/bridge/#:~:text=In%20terms%20of%20Docker%2C%20a,connected%20to%20that%20bridge%20network.&text=Bridge%20networks%20apply%20to%20containers%20running%20on%20the%20same%20Docker%20daemon%20host.

> Docker network bridge will create another layer on your network interface. This will make the container become isolated. Each container with same network bridge, will be able to connected

If you want to connect to local machine db server with bridge network, you need to change the bind-address to `0.0.0.0` of your db server.

## Before binding to any port
Make sure that your database user not listening to any host

## Bind port to `0.0.0.0`
By binding to port 0.0.0.0, db server will listen to any port, thus remote connection.

### For linux
Find your mariadb config file. usually located at
`/etc/mysql/mariadb.conf.d/50-server.cnf`

## Configure
after that, configure the db: 
https://mariadb.com/kb/en/configuring-mariadb-for-remote-client-access/

## Setup your firewall
if using linux
- https://www.cyberciti.biz/faq/how-to-block-an-ip-address-with-ufw-on-ubuntu-linux-server/
- https://serverfault.com/questions/484475/ufw-firewall-rules-ordering

`sudo ufw status numbered`
```  
Status: active

     To                         Action      From
     --                         ------      ----
[ 1] Anywhere                   ALLOW IN    127.0.0.0                 
[ 2] Anywhere                   ALLOW IN    172.17.0.0/16             
[ 3] Anywhere                   ALLOW IN    192.168.0.0/16            
[ 4] Anywhere                   DENY IN     Anywhere                  
[ 5] Anywhere (v6)              DENY IN     Anywhere (v6) 
```

change the bind address value to `0.0.0.0`

## Using it
### host machine
`ip addr` or `sudo ip addr show docker0`
```
6: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:aa:24:86:92 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:aaff:fe24:8692/64 scope link 
       valid_lft forever preferred_lft forever
```

### set env to use docker ip gateway
```
DB_CONNECTION=mysql
DB_HOST=172.17.0.1
DB_PORT=3306
DB_DATABASE=mobilepulsa
DB_USERNAME=root
DB_PASSWORD=
```

### container machine
Checking the ip translation to our host machine.

`ip addr`
```
230: eth0@if231: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:c0:a8:a0:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 192.168.160.2/20 brd 192.168.175.255 scope global eth0
       valid_lft forever preferred_lft forever
```

### allowing ip translation to db driver
```
+------------------+-------------+
| User             | Host        |
+------------------+-------------+
| root             | 192.168.%.% |
+------------------+-------------+
```

### docker container able to connect to host machine db
Go inside your db container
```
root@db:/# mysql -h 172.17.0.1 -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 102
Server version: 10.3.31-MariaDB-0ubuntu0.20.04.1 Ubuntu 20.04

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show databases;
+-------------------------+
| Database                |
+-------------------------+
| finance                 |
| information_schema      |
| mobilepulsa             |
| mobilepulsa__cms        |
| mobilepulsa_cms_testing |
| mobilepulsa_log         |
| mobilepulsa_testing     |
| mysql                   |
| performance_schema      |
| sendportal              |
| wordpress               |
+-------------------------+
11 rows in set (0.001 sec)

MariaDB [(none)]> 
```

# Option 2: with docker host network
https://docs.docker.com/network/host/#:~:text=The%20host%20networking%20driver%20only,the%20docker%20service%20create%20command.

> Docker network host making your docker container using the network at host machine

When running the container, just set the network mode using `host` and your container will be using the same network layer with host machine

```
network_mode: host
```