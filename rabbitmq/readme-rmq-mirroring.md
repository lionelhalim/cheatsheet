# Types of mirroring in RabbitMQ
Ketika sebuah node rmq tergabung ke dalam cluster. Exchange dan bindings akan selalu ada di tiap node dalam cluster tersebut. Berbeda dengan konten queue yang hanya akan disimpan pada satu node dalam sebuah cluster. Untuk dapat mereplika konten queue tersebut ke semua node dalam satu cluster, digunakan yang namanya mirroring.

Terdapat 2 jenis mirroring

1. Classic queue mirroring
    - Consists of leader replica and mirror
    - Ketika leader down, oldest mirror akan naik menjadi leader
2. Quorum Queues
    - lebih fokus ke data safety
    - menggunakan Raft conses algorithm: https://raft.github.io/

## Basic Queue Mirroring
https://www.rabbitmq.com/ha.html#mirroring-arguments
```
docker exec -it rabbit-1 bash

rabbitmqctl set_policy ha-fed \
    ".*" '{"federation-upstream-set":"all", "ha-mode":"nodes", "ha-params":["rabbit@rabbit-1","rabbit@rabbit-2","rabbit@rabbit-3"]}' \
    --priority 1 \
    --apply-to queues
```

## Enable automatic synchronizing
https://www.rabbitmq.com/ha.html#unsynchronised-mirrors
```
rabbitmqctl set_policy ha-fed \
    ".*" '{"federation-upstream-set":"all", "ha-sync-mode":"automatic", "ha-mode":"nodes", "ha-params":["rabbit@rabbit-1","rabbit@rabbit-2","rabbit@rabbit-3"]}' \
    --priority 1 \
    --apply-to queues
```

## Plugin rabbitmq_federation
Enable plugin ini supaya policynya bisa digunakan
```
docker exec -it rabbit-1 rabbitmq-plugins enable rabbitmq_federation 
docker exec -it rabbit-2 rabbitmq-plugins enable rabbitmq_federation
docker exec -it rabbit-3 rabbitmq-plugins enable rabbitmq_federation
```