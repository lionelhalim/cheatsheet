# Implementation Explanation for rabbitmq with docker

## Creating docker network
By creating local docker network, each local docker container will be able to interconnected
```
docker network create rabbits
```

## Run a standalone instance
- -d is run in background
- --rm is removing container if delete
- --net running the container using the rabbit network
```
docker run -d --rm --net rabbits --hostname rabbit-1 --name rabbit-1 rabbitmq:3.8 
```

## How to grab existing erlang cookie
For each node inside rabbit cluster, it is required to have the same .erlang.cookie for them to be able to synchronized and authenticated with each other.
- Cookie Example: FFKZCAHFJLBAUOCFQNEC
```
docker exec -it rabbit-1 cat /var/lib/rabbitmq/.erlang.cookie
```

## Clean up
```
docker rm -f rabbit-1
```

# Enabling RabbitMQ Management
```
docker run -d --rm --net rabbits -p 8080:15672 --hostname rabbit-manager --name rabbit-manager rabbitmq:3-management
```

# Clustering
## 1.A. With erlang cookie explicitly given in environment
This method has been deprecated at rabbitmq version 3.8 and removed at 3.9
- -e is setting environment
```
docker run -d --rm --net rabbits -p 8080:15672 -e RABBITMQ_ERLANG_COOKIE=FFKZCAHFJLBAUOCFQNEC --hostname rabbit-manager --name rabbit-manager rabbitmq:3.8-management
```

## 1.B. Multiple container with docker swarm (docker orchestration tool)
### Creating a secret for erlang cookie
```
printf "alphanumerickeyforerlangcookie" | docker secret create my_secret -
```

### Creating overlay network for services
```
docker network create --network overlay rabbits-network
```

### Creating docker service with swarm
- uid 0999 is rabbitmq user id
- gid 0999 is rabbitmq group id
- mode 0600 to make .erlang.cookie readable and writable
```
docker service create -d \
   --network rabbits-network \
   -p 8081:15672 \
   --hostname rabbit \
   --name rabbit \
   -- replicas 3 \
   --secret source=erlang_cookie,target=/var/lib/rabbitmq/.erlang.cookie,uid=0999,gid=0999,mode=0600 \
   rabbitmq:3-management
```

## 1.C. Multiple container without docker swarm (with docker-compose)
- Cek file docker-compose.yaml pada folder ini


## 2. After the container is up and running
https://www.rabbitmq.com/configure.html

In this case, the three rabbitmq container that is created will join the cluster of rabbit-1
```
#join the cluster

docker exec -it rabbit-manager rabbitmqctl stop_app
docker exec -it rabbit-manager rabbitmqctl reset
docker exec -it rabbit-manager rabbitmqctl join_cluster rabbit@rabbit-1
docker exec -it rabbit-manager rabbitmqctl start_app
docker exec -it rabbit-manager rabbitmqctl cluster_status
```